const getCube = 2 ** 3;
let messageCube = 'The cube of 2 is '
console.log(messageCube + getCube);

					
const fullAddress = ["BLK 6", "LOT 3", "Phase 1", "Roxaco", "Landing", "Subdivision", "4231"];
console.log(`I live at ${fullAddress[0]} ${fullAddress[1]} ${fullAddress[2]} ${fullAddress[3]}, ${fullAddress[4]} ${fullAddress[5]}, with a zip code of ${fullAddress[6]} !`);

const animal = {
	animalName: "Lolong",
	animalKind: "saltwater crocodile",
	animalWeight: "1075 kgs",
	animalMeasurement: "20 ft 3 in"
};

const { animalName, animalKind, animalWeight, animalMeasurement } = animal;

function getAnimalFullInfo ({animalName, animalKind, animalWeight, animalMeasurement}) {
	console.log(`${animalName} was a ${animalKind}. He wighed at ${animalWeight} with a measurement of ${animalMeasurement}.`)
}
getAnimalFullInfo(animal)


const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => {
	console.log(`${number}`)
})

const addition = (a, b, c, d, e) => a + b + c + d + e ;
let resultOfAddition = addition(1, 2, 3, 4, 5)
console.log(resultOfAddition)

class Dog {
	constructor(name, age, breed) { 
		this.name = name
		this.age = age
		this.breed = breed
	};
};

const myDog = new Dog();

myDog.name = "Frankie"
myDog.age = 5
myDog.breed = "Miniature Dachshund"

console.log(myDog)
